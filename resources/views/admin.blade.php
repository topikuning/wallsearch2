<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ url('admin_asset/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="{{ url('admin_asset/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{ url('admin_asset/dist/css/skins/skin-blue.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ url('admin_asset/plugins/iCheck/flat/blue.css') }}" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="{{ url('admin_asset/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="{{ url('admin_asset/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ url('admin_asset/plugins/datatable/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ url('admin_asset/plugins/dialog/bootstrap-dialog.min.css') }}">
    <link href="{{ url('admin_asset/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ url('admin_asset/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('admin_asset/plugins/switch/switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('admin_asset/plugins/editable/css/bootstrap-editable.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('admin_asset/admin.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

  <!-- jQuery 2.1.3 -->
    <script src="{{ url('admin_asset/plugins/jQuery/jQuery-2.1.3.min.js') }}"></script>
    <!-- jQuery UI 1.11.2 -->
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
 
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ url('admin_asset/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>  

    <script type="text/javascript" src="{{ url('admin_asset/bower_components/moment/min/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_asset//bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- Morris.js charts -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{ url('admin_asset/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="{{ url('admin_asset/plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ url('admin_asset/plugins/knob/jquery.knob.js') }}" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="{{ url('admin_asset/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>

    <!-- iCheck -->
    <script src="{{ url('admin_asset/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="{{ url('admin_asset/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="{{ url('admin_asset/plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="{{ url('admin_asset/plugins/datatable/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin_asset/plugins/datatable/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    
    <script type="text/javascript" src="{{ url('admin_asset/plugins/dialog/bootstrap-dialog.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_asset/plugins/select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_asset/plugins/switch/switch.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_asset/plugins/editable/js/bootstrap-editable.min.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('admin_asset/dist/js/app.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
      var base_url = "{{ url('/') }}";
    </script>

    <style type="text/css">
      .editable-click{cursor: pointer;}
    </style>

  </head>
  <body class="skin-blue fixed">
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo"><b>Panel Admin</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ url('admin_asset/images/mario.png') }}" class="user-image" alt="User Image"/>
                  <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{ url('admin_asset/images/mario.png') }}" class="img-circle" alt="User Image" />
                    <p>
                      {{ Auth::user()->name }} Admin
                      <small>Member since {{ Auth::user()->created_at }}</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{ url('/badmin/profile') }}" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin._nav')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        @yield('content')
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 0.1
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="#">Wirosableng Corp</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    <script>
      $.widget.bridge('uibutton', $.ui.button);

      window._loading = '<div class="overlay" id="ajax-loading"><i class="fa fa-refresh fa-spin"></i></div>';
      $('div.overlay-message').delay(5000).slideUp();
    </script>


  </body>
</html>