{!! '<?xml version="1.0" encoding="UTF-8" ?>' !!}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ url('/') }}</loc>
        <priority>1</priority>
        <changefreq>always</changefreq>
    </url>


    @foreach($pages as $page)
    <url>
        <loc>{{ url('/page/'. $page->slug) }}.html</loc>
        <priority>0.5</priority>
        <changefreq>monthly</changefreq>
    </url>
    @endforeach


    @foreach($categories as $category)
    <url>
        <loc>{{ url('/category/'. $category->slug) }}</loc>
        <priority>0.7</priority>
        <changefreq>daily</changefreq>
    </url>
    @endforeach

</urlset>