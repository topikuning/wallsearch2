<footer class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row animate fadeInUp">
        <div class="col-md-3">
          <div class="footer-widget clearfix">
            <h4>About Us</h4>
            <ul>
				@foreach($pages as $page)
					<li><a href="{{ ('/page/'. $page->slug) }}.html">{{ $page->title }}</a></li>
				@endforeach
				@if(isset($robot))
					<li><a rel="follow,noindex" href="{{ ('/index.xml') }}">Sitemaps</a></li>
				@endif
            </ul>
          </div>
        </div>

        <div class="col-md-9">
          <div class="footer-widget clearfix">
            <h4>Last Searched</h4>
            <div class="footer-tags">
              <span class="tag-links">
				@foreach($last_viewed as $viewed)
					<a href="{{ ('/category/'.$viewed->slug_keyword) }}.html" rel="index,follow">{{ $viewed->keyword }}</a>
				@endforeach
              </span>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p class="copyright">Copyright &#169; 2016
              <a rel="follow" href="/">{{ config('site.site_footer') }}</a></p>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<a href="#" class="hc_scrollup"><i class="fa fa-chevron-up"></i></a>
@include('partials._script')
@if(config('site.histat_id') !== "")
<!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,{{ config('site.histat_id') }},4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('http://s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?{{ config('site.histat_id') }}&101" alt="hit counter" border="0"></a></noscript>
<!-- Histats.com  END  -->
@endif
