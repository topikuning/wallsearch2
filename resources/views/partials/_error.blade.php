<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>404 Page Not Found - {{ config('site.site_url') }}</title>

  <link href="/wp-content/themes/nowyouseeme/404/common_o2.1-pretzel-63569659624b0120e962749578cae707.css" media="all" rel="stylesheet" type="text/css">

  <link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/nowyouseeme/404/logotype_favicon_pretzel-114df7f43fae7dd6dbc4ab074d934da5.ico">

  <!-- Replace by loading from stylesheet -->
  <style type="text/css">
    .text-ginormous {
      font-size: 145px;
    }

    .navbar .container.fixed-height {
      height: 46px;
    }

    .brand.airbnb.center {
      float: none;
      margin: 7px auto 0 auto;
    }
  </style>
</head>

<body>
  <div class="page-container page-container-responsive">
    <div class="row space-top-8 space-8 row-table">
        <div class="col-5 col-middle">
          <h1 class="text-jumbo text-ginormous">Oops!</h1>
          <h2>We can't seem to find the page you're looking for.</h2>
          <h6>Error code: 404</h6>
          <ul class="list-unstyled">
            <li>Here are some helpful links instead:</li>
            <li><a href="/">Home</a></li>
			<?php $itung=1; ?>
			@foreach($last_viewed as $viewed)
				@if($itung < 7)
					<li><a href="{{ ('/category/'.$viewed->slug_keyword) }}.html" rel="follow">{{ $viewed->keyword }}</a></li>
				@endif
				<?php $itung++; ?>
			@endforeach
          </ul>
        </div>
        <div class="col-5 col-middle text-center">
          <img src="/wp-content/themes/nowyouseeme/404/404-Airbnb_final-554adc9e2190445fea6d7f0e3a30e67e.gif" width="313" height="428" alt="{{ config('site.site_title') }}.">
        </div>
      </div>
    </div>
  </div>
</body>
</html>