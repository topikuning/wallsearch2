              <div class="right-sidebar-block span3">
                  <div class="sidepanel widget_search">
                      <div class="search_form_block">
                          <form name="search_form" method="get" action="{{ url('/search') }}" class="search_form">
                              <input type="text" name="q" placeholder="Search" @if(isset($search)) value="{{ $search }}" @endif
                                     title="Search" class="field_search">
                              <input type="submit" name="submit_search" value="Search" title="" class="s_btn_search">
                              <i class="icon-search"></i>
                              <div class="clear"></div>
                          </form>
                      </div>
                  </div>
                  <div class="sidepanel widget_categories">
                      <h4 class="sidebar_heading">Categories</h4>
                      <ul>
                        @foreach($categories as $row)
                        <li><a href="{{ url('category/'. $row->slug) }}">{{ $row->title }}</a></li>
                        @endforeach
                      </ul>
                  </div>

                  <div class="sidepanel widget_posts">
                      <h4 class="sidebar_heading">Last Viewed</h4>
                      <ul class="recent_posts">  
                        <?php $list_side = $last_viewed; ?>
                        @include('partials._sidebar_post')
                      </ul>
                  </div>


                  <div class="sidepanel widget_posts">
                      <h4 class="sidebar_heading">Best Rated</h4>
                      <ul class="recent_posts">  
                        <?php $list_side = $best_rated; ?>
                        @include('partials._sidebar_post')
                      </ul>
                  </div>
              </div>