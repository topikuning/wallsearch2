<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
	@if(!isset($current_title))
		<title>{{ config('site.site_title') }}</title>
	@else
		<title>{{ $current_title }} | {{ config('site.site_title') }}</title>
	@endif
	
	@if(config('site.webmaster_id') !== "")
		<meta name="google-site-verification" content="{{ config('site.webmaster_id') }}" />
	@endif

	@if(config('site.bing_id') !== "")
		<meta name="msvalidate.01" content="{{ config('site.bing_id') }}" />
	@endif

	@if(!isset($no_description))
		@if(!isset($current_description))
		<meta name="description" content="{{ config('site.site_desc') }}">
		@else
			<meta name="description" content="{{ $current_description }}">
		@endif
	@endif

	@if(isset($noindex))
		<meta name="robots" content="noindex,follow" />
	@else
		<meta name="robots" content="index,follow" />
	@endif
	
	<link rel='stylesheet' href="{{ ('/wp-content/themes/nowyouseeme/css/bootstrap.css')}}" type='text/css' media='all' />
	<link rel='stylesheet' href="{{ ('/wp-content/themes/nowyouseeme/style.css')}}" type='text/css' media='all' />
	<link rel='stylesheet' href="{{ ('/wp-content/themes/nowyouseeme/css/photobox.css')}}" type='text/css' media='all' />
	<link rel='stylesheet' href="{{ ('/wp-content/themes/nowyouseeme/css/animate.min.css')}}" type='text/css' media='all' />
	<link rel='stylesheet' href="{{ ('/wp-content/themes/nowyouseeme/css/animations.min.css')}}" type='text/css' media='all' />
	<link rel='stylesheet' href="{{ ('/wp-content/themes/nowyouseeme/css/font/font.css')}}" type='text/css' media='all' />
	<link rel='stylesheet' href="//fonts.googleapis.com/css?family=Bitter%3A400%2C600%2C700%2C800%2C300%7CFira+Sans%3A300%2C400%2C500%2C700%2C400italic%2C300italic&#038;ver=4.4.2" type='text/css' media='all' />
	<link rel='stylesheet' href="{{ ('/wp-content/themes/nowyouseeme/css/font-awesome-4.4.0/css/font-awesome.min.css')}}" type='text/css' media='all' />
	<script type="text/javascript">
	var base_url = "{{ url('/') }}";
	</script>
</head>