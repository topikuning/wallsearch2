@foreach($listing as $row)
	<?php
		$url = ('/wp-content/uploads/thumb/'. $row->slug . '.jpg');
	?>
	<div class="col-sm-4 col-md-3 masonry-item">
		<div class="post-box">
			<a href="{{ ($row->slug.'-'.$row->id) }}.html">
				<img class="img-responsive" src="{{ $url }}" alt="{{ $row->keyword }}" title="{{ $row->keyword }}"/>
				<h2>{{ $row->title }}</h2>
			</a>
		</div>
	</div>
@endforeach