	<div class="header-section">
      <div class="container header-inner">
        <div class="row">
          <div class="col-md-6 site-logo">
            <h2>
              <a href="/" title="{{ config('site.site_title') }}">
								<img src="/wp-content/themes/nowyouseeme/images/logo.png" alt="{{ config('site.site_title') }}" class="img-responsive" />
							</a>
            </h2>
          </div>
          <div class="col-md-6">

          </div>
        </div>
      </div>
      <div class="container">
        <nav class="navbar navbar-default">
          <div class="row">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <div class="">
                <ul class="nav navbar-nav">
					@foreach($pages as $page)
					<li><a href="{{ ('/page/'. $page->slug) }}.html">{{ $page->title }}</a></li>
					@endforeach
                </ul>
              </div>
			  
			  <form action="/search" method="get" id="searchform">
        				<div class="col-md-2 col-sm-10 col-xs-10 input-group search-box-top pull-right">
        				<input type="text" class="form-control" name="s" id="s"  aria-describedby="basic-addon2" placeholder="Search Here" />
        			  </div>
        			</form>
            </div>
          </div>
        </nav>
      </div>
    </div>