<script type="text/javascript" src="{{ ('/wp-content/themes/nowyouseeme/js/jquery-1.11.0.js') }}"></script>
<script type="text/javascript" src="{{ ('/wp-content/themes/nowyouseeme/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ ('/wp-content/themes/nowyouseeme/js/carousel.js') }}"></script>
<script type="text/javascript" src="{{ ('/wp-content/themes/nowyouseeme/js/animations.js') }}"></script>
<script type="text/javascript" src="{{ ('/wp-content/themes/nowyouseeme/js/jquery.photobox.js') }}"></script>
<script type="text/javascript" src="{{ ('/wp-content/themes/nowyouseeme/js/page-scroll.js') }}"></script>

<script type="text/javascript" src="{{ ('/wp-content/themes/nowyouseeme/js/imagesloaded.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ ('/wp-content/themes/nowyouseeme/js/masonry.pkgd.min.js') }}"></script>

<script type="text/javascript">
/* =Masonry ===================================== */
var $msnry_container = jQuery('.masonry-container');
// initialize Masonry after all images have loaded
$msnry_container.imagesLoaded( function() {
  $msnry_container.masonry({
    itemSelector: '.masonry-item'
  });
});
</script>
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"/page/privacy-policy.html","theme":"dark-floating"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->