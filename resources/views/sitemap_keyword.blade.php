{!! '<?xml version="1.0" encoding="UTF-8" ?>' !!}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($keywords as $keyword)
    <url>
        <loc>{{ url('/'. $keyword->slug_keyword) }}</loc>
        <priority>0.6</priority>
        <changefreq>weekly</changefreq>
    </url>
    @endforeach
</urlset>