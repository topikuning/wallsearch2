@extends('admin')
@section('content')
<?php 
    function getRecord($record, $value){
        if(old($value) != null) return old($value);
        return $record->$value;
    }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    @if($record->id)
    Edit Page
    @else
    New Page
    @endif
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/badmin/pages') }}"><i class="fa fa-dashboard"></i>Pages</a></li>
    <li class="active">Pages</li>
  </ol>
</section>

<section class="content">
    @include('partials._message')
    @include('partials._error')
    <div class="row">

      <form action="{{ url('badmin/pages/create') }}" method="post" role="form">
        <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Page</h3>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="id" value="{{ $record->id }}">
                        <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" class="form-control" name="title" placeholder="title" value="{{ getRecord($record,'title') }}">
                        </div>
                        <div class="form-group">
                            <label for="">Slug</label>
                            <input type="text" class="form-control" name="slug" placeholder="slug" value="{{ getRecord($record,'slug') }}" @if($record->id)disabled @endif>
                        </div>
                        <div class="form-group">
                            <label for="">Content</label>
                            <textarea class="form-control" name="body" rows="10" id="content_body">{!! getRecord($record,'body') !!}</textarea>
                        </div>        
                    </div>

                    <div class="box-footer clearfix">
                      <div class="pull-right">
                        <button class="btn bg-maroon btn-flat margin" name="submit" value="0" type="submit">Draft</button>
                        <button class="btn bg-olive btn-flat" name="submit" value="1">Publish</button>
                      </div>
                    </div>
                </div>

        </div>
      </form>
    </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){
        @if(!$record->id)
        $('input[name="title"]').blur(function(){
           var val = $(this).val().trim(); 
            if(val){
                $('input[name="slug"]').attr('disabled', true); 
                $.get(base_url + '/badmin/createSlug', {text:val}, function(res){
                    $('input[name="slug"]').removeAttr('disabled');
                    $('input[name="slug"]').val(res);
                });
            }
        });
        @endif

        CKEDITOR.replace('content_body');
  });
</script>

@endsection