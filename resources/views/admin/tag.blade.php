@extends('admin')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Tags
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active">Tags</li>
  </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    
                    <a href="#" class="btn btn-sm btn-info" id="add_new"><i class="fa fa-plus"></i> Add New</a>
                </div>
                <div class="box-body">
                    <table class="table table-hover" id="tag_table">
                        <thead>
                            <tr>
                                <th style="width:30px;">ID</th>
                                <th>Tag</th>
                                <th>Slug</th>
                                <th style="width:75px;">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">

$(document).ready(function() {
    /**table processing **/
    $('#tag_table').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax":  base_url + "/badmin/tags/read",
          "aoColumnDefs": [
              { 'bSortable': false, 'aTargets': [3] }
           ]
    });

    function createRow(text){
        var json = JSON.parse(text); 
        var tds = []; 
        tds.push($('<td>').text(json.id)); 
        tds.push($('<td>').text(json.name));
        tds.push($('<td>').text(json.slug));
        tds.push($('<td>').html(
            '<span class="badge bg-green edit-inline">Edit</span>' + 
            '<span class="badge bg-red delete-inline">Delete</span>'
        ));
        return tds;
    }

    $('#add_new').click(function(e){
        e.preventDefault();
        var div = $('<div>').load(base_url + "/badmin/tags/create", function(){
            div.find('input[name="name"]').blur(function(){
               var val = $(this).val().trim(); 
                if(val){
                    div.find('input[name="slug"]').attr('disabled', true); 
                    $.get(base_url + '/badmin/createSlug', {text:val}, function(res){
                        div.find('input[name="slug"]').removeAttr('disabled');
                        div.find('input[name="slug"]').val(res);
                    });
                }
            });
        });
        BootstrapDialog.show({
            title:'Add new',
            message:div,
            buttons:[
                {
                    label:'Save',
                    cssClass:'btn-success',
                    action:function(dialog){
                        var data = dialog.getModalBody().find('form').serializeArray(); 
                        var box = dialog.getModalBody().find('div.box'); 
                        box.append(_loading); 
                        var me = $(this);
                        me.addClass('disabled');
                        $.post(base_url + '/badmin/tags/create', data, function(res){
                            var tds = createRow(res); 
                            var tr = $('<tr>').html(tds); 
                            tr.prependTo('#tag_table > tbody');
                            dialog.close();
                        })
                        .error(function(res){
                            me.removeClass('disabled'); 
                            $('#ajax-loading').remove();
                            var json = res.responseText;
                            BootstrapDialog.alert(json); 
                        });


                    }
                }
            ]
        });
    }); 



    /**delete table**/
    $('#tag_table').on('click', '.delete-inline', function(e){
        var tr = $(this).closest('tr'); 
        var id = tr.find('td').get(0);
        id = $(id).text(); 
        BootstrapDialog.confirm('Are you sure delete this?', function(res){
            if(res){
                $.ajax({
                    url:base_url + '/badmin/tags/' + id, 
                    method:'DELETE', 
                    success:function(res){
                        if(res) tr.remove();
                    }
                })
            }
        });
    });


    /**edit table **/
    $('#tag_table').on('click', '.edit-inline', function(e){
        var tr = $(this).closest('tr'); 
        var id = tr.find('td').get(0);
        id = $(id).text(); 

        var div = $('<div>').load(base_url + "/badmin/tags/edit/" + id, function(){

            div.find('input[name="slug"]').attr('disabled', true);

            /**
            div.find('input[name="name"]').blur(function(){
               var val = $(this).val().trim(); 
                if(val){
                    div.find('input[name="slug"]').attr('disabled', true); 
                    $.get(base_url + '/badmin/createSlug', {text:val}, function(res){
                        div.find('input[name="slug"]').removeAttr('disabled');
                        div.find('input[name="slug"]').val(res);
                    });
                }
            });
            **/
        });

        BootstrapDialog.show({
            title:'Edit',
            message:div,
            buttons:[
                {
                    label:'Save',
                    cssClass:'btn-success',
                    action:function(dialog){
                        var data = dialog.getModalBody().find('form').serializeArray(); 
                        var box = dialog.getModalBody().find('div.box'); 
                        box.append(_loading); 
                        var me = $(this);
                        me.addClass('disabled');
                        $.post(base_url + '/badmin/tags/edit/' + id, data, function(res){
                            var tds = createRow(res); 
                            tr.html(tds);
                            dialog.close();
                        })
                        .error(function(res){
                            me.removeClass('disabled'); 
                            $('#ajax-loading').remove();
                            var json = res.responseText;
                            BootstrapDialog.alert(json); 
                        });


                    }
                }
            ]
        });
    });  



});
    


</script>


@endsection