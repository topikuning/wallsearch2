@extends('admin')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Categories
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active">Categories</li>
  </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-warning" id="tree_list">
                @include('partials._treecat')
            </div>
        </div>
        <div class="col-md-6" id="box_cat">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new root category</h3>
                </div>
                <div class="box-body">
                    <form action="#" role="form" method="post">
                        <input type="hidden" name="parent_id" value="0">
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Category name">
                        </div>
                        <div class="form-group">
                            <label for="">Slug</label>
                            <input type="text" name="slug" class="form-control" placeholder="Slug url">
                        </div>
                    </form>
                </div>
                <div class="box-footer clearfix">
                    <button id="newcat" class="pull-right btn btn-success">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function(){

        var div = $('#box_cat').find('form'); 
        div.find('input[name="name"]').blur(function(){
           var val = $(this).val().trim(); 
            if(val){
                div.find('input[name="slug"]').attr('disabled', true); 
                $.get(base_url + '/badmin/createSlug', {text:val}, function(res){
                    div.find('input[name="slug"]').removeAttr('disabled');
                    div.find('input[name="slug"]').val(res);
                });
            }
        });


        $('#newcat').click(function(){
            var data = $('#box_cat').find('form').serializeArray(); 
            $('#box_cat').find('.box.box-danger').append(_loading);

            $.post(base_url + '/badmin/categories/create', data, function(res){
                //success function
                $('#ajax-loading').remove();
                $('#tree_list').append(_loading); 
                $('#tree_list').load(base_url + '/badmin/categories/tree', function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });

            })
            .error(function(res){ 
                $('#ajax-loading').remove();
                var json = res.responseText;
                BootstrapDialog.alert(json); 
            });
        });

        $('#tree_list').on('click','.inline-delete', function(e){
            var li = $(this).closest('li');
            BootstrapDialog.confirm('Remove this?', function(res){
                if(res){
                    var id = li.attr('data-id'); 
                    $('#tree_list').append(_loading);
                    $.ajax({
                        url:base_url + '/badmin/categories/destroy/' + id,
                        method:'DELETE', 
                        success:function(res){
                            $('#ajax-loading').remove();
                            li.next('ul').remove(); 
                            li.remove();
                        }
                    });
                }
            });
        });

        $('#tree_list').on('click','.inline-edit', function(e){
            var li = $(this).closest('li'); 
            var box = $('#box_cat').find('.box').clone(); 
            box.find('.box-header').remove();
            box.find('.box-footer').remove();
            box.find('input[name="slug"]').attr('disabled', true);

            var id = li.attr('data-id'); 
            var name = li.find('.text-label').text().trim(); 
            var slug = li.attr('data-slug'); 

            box.find('input[name="name"]').val(name); 
            box.find('input[name="slug"]').val(slug);
            
            BootstrapDialog.show({
                title:'Edit Category', 
                message:box,
                buttons:[
                    {
                        label:'Save', 
                        cssClass:'btn-success', 
                        action:function(dialog){
                            var data = box.find('form').serializeArray(); 
                            var me = $(this);
                            me.addClass('disabled');
                            box.append(_loading);
                            $.post(base_url + '/badmin/categories/edit/' + id, data, function(res){
                                var xdata = JSON.parse(res);
                                li.find('.text-label').text(xdata.name);
                                dialog.close();
                            })
                            .error(function(res){
                                me.removeClass('disabled'); 
                                $('#ajax-loading').remove();
                                var json = res.responseText;
                                BootstrapDialog.alert(json); 
                            });                         

                        }
                    }
                ]
            });
        });

        $('#tree_list').on('click','.inline-add', function(e){
            var li = $(this).closest('li'); 
            var box = $('#box_cat').find('.box').clone(); 
            box.find('.box-header').remove();
            box.find('.box-footer').remove();
            var id = li.attr('data-id'); 
            box.find('input[name="name"]').val(""); 
            box.find('input[name="slug"]').val("");
            box.find('input[name="parent_id"]').val(id);

            box.find('input[name="name"]').blur(function(){
               var val = $(this).val().trim(); 
                if(val){
                    box.find('input[name="slug"]').attr('disabled', true); 
                    $.get(base_url + '/badmin/createSlug', {text:val}, function(res){
                        box.find('input[name="slug"]').removeAttr('disabled');
                        box.find('input[name="slug"]').val(res);
                    });
                }
            });
            
            BootstrapDialog.show({
                title:'Add Child Category', 
                message:box,
                buttons:[
                    {
                        label:'Save', 
                        cssClass:'btn-success', 
                        action:function(dialog){
                            var data = box.find('form').serializeArray(); 
                            var me = $(this);
                            me.addClass('disabled');
                            box.append(_loading);
                            $.post(base_url + '/badmin/categories/createnode', data, function(res){
                                var next = li.next(); 
                                if (next.prop('tagName') =='UL'){
                                    next.append(res);
                                }else{
                                    $('<ul>').append(res).insertAfter(li); 
                                }
                                $('[data-toggle="tooltip"]').tooltip();
                                dialog.close();
                            })
                            .error(function(res){
                                me.removeClass('disabled'); 
                                $('#ajax-loading').remove();
                                var json = res.responseText;
                                BootstrapDialog.alert(json); 
                            });                         

                        }
                    }
                ]
            });
        });
    });
</script>

@endsection