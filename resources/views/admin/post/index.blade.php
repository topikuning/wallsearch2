@extends('admin')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Posts
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active">Posts</li>
  </ol>
</section>

<section class="content">
    @include('partials._message')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info" id="box_post">
                <div class="box-header with-border">
                    <a href="{{ url('badmin/posts/create') }}" class="btn btn-sm btn-info" id="add_new"><i class="fa fa-plus"></i> Add New</a>
                </div>
                <div class="box-body">
                    <table class="table table-hover" id="post_table">
                        <thead>
                            <tr>
                                <th style="width:30px;">ID</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Abstract</th>
                                <th>Viewed</th>
                                <th style="width:75px;">Download</th>
                                <th style="width:75px;">Published</th>
                                <th style="width:110px;">Created at</th>
                                <th style="width:75px;">PDF File?</th>
                                <th style="width:75px;">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function(){


            function editAttribute(id,field,value){
                $('#box_post').append(_loading);
                var url = base_url + '/badmin/posts/attribute'; 
                var params = {
                    id:id,
                    field:field,
                    value:value
                };
                $.post(url,params, function(res){
                    $('#ajax-loading').remove();
                });
            }


            /**table processing **/
            $('#post_table').dataTable({
                "processing": true,
                "serverSide": true,
                "ajax":  base_url + "/badmin/posts/read",
               "columnDefs":[
                    { 'bSortable': false, 'aTargets': [9] },
                    {
                        render:function(data, type, row){
                            return '<a href="#">' + data + '</a>'; 
                        }, 
                        "targets":1
                    },
                    {
                         render: function ( data, type, row ) {
                            return '<span data-id="'+row[0]+'" class="label bg-green edit-view">' + data + '</span>';
                         },

                         "targets": 4
                    },
                    {
                         render: function ( data, type, row ) {
                            return '<span data-id="'+row[0]+'" class="label bg-red edit-download">' + data + '</span>';
                         },

                         "targets": 5
                    },
                    {
                         render: function ( data, type, row ) {

                            if(data =="1")
                            return '<input data-id="'+row[0]+'" type="checkbox" name="published" checked>';
                            return '<input data-id="'+row[0]+'" type="checkbox" name="published">';
                         },

                         "targets": 6
                    },
                    {
                        render: function(data, type, row){
                            return '<span data-id="'+row[0]+'" class="edit-created">' + data + '</span>';
                        }, 
                        "targets": 7
                    },
                    {
                        render: function(data, type, row){
                            var url = base_url + '/badmin/posts/download/' + data;
                            if(data.trim()) return '<a href="'+ url +'" title="'+ data +'"><i class="fa fa-file-o"></i></a>'; 
                            return 'none';
                        },
                        "targets":8
                    }
               ],
               "fnDrawCallback":function(){
                   $('input[name="published"]').bootstrapSwitch({size:'mini'});
                    $('input[name="published"]').on('switchChange.bootstrapSwitch', function(event, state) {
                      var value = state?"1":"0"; 
                      var id = $(this).attr('data-id');
                      editAttribute(id,'published', value);
                    });


                    $('span.edit-view').editable({
                        validate: function(value) {
                           if($.trim(value) == '') return 'empty value disallowed';
                           if(isNaN(value)) return 'non numeric value disallowed';
                        },
                        success:function(res,newVal){
                            var id = $(this).attr('data-id'); 
                            editAttribute(id,'viewed', newVal);
                        }
                    });

                    $('span.edit-download').editable({
                        validate: function(value) {
                           if($.trim(value) == '') return 'empty value disallowed';
                           if(isNaN(value)) return 'non numeric value disallowed';
                        },
                        success:function(res,newVal){
                            var id = $(this).attr('data-id'); 
                            editAttribute(id,'download', newVal);
                        }
                    });

                    $('span.edit-created').editable({
                        validate:function(value){
                            if($.trim(value) == '') return 'empty value disallowed';
                        },
                        success:function(res,newVal){
                            var id = $(this).attr('data-id'); 
                            editAttribute(id,'created_at', newVal);
                        }
                    }).on('shown', function(e, editable){
                        editable.input.$input.datetimepicker({format:'YYYY-MM-DD HH:mm:ss'});
                    });
               }

            });

            /**
            $('#post_table').on('click', 'span.edit-inline', function(){
                var id = $(this).attr('data-id'); 
                document.location.href = base_url + '/badmin/posts/edit/' + id;

            });
            **/


        /**delete table**/
        $('#post_table').on('click', '.delete-inline', function(e){
            var tr = $(this).closest('tr'); 
            var id = $(this).attr('data-id');
            BootstrapDialog.confirm('Are you sure delete this?', function(res){
                if(res){
                    $('#box_post').append(_loading);
                    $.ajax({
                        url:base_url + '/badmin/posts/' + id, 
                        method:'DELETE', 
                        success:function(res){
                            if(res){
                              tr.remove();
                              $('#ajax-loading').remove();  
                            } 
                        }
                    });
                }
            });
        });

    });

</script>

@endsection