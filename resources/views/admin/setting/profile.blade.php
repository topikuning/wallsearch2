@extends('admin')
@section('content')
<?php 
    function getRecord($record, $value){
        if($record==null) return old($value);
        return $record->$value;
    }
?>

<section class="content-header">
  <h1>
        Settings
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/badmin') }}"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active">Profile</li>
  </ol>
</section>

<section class="content">
    @include('partials._message')
    @include('partials._error')

    <div class="row">
        <form action="{{ url('badmin/profile') }}" method="post">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Profile Settings</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="" value="{{ getRecord($record,'email')}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ getRecord($record,'name')}}">
                        </div>
                        <div class="form-group">
                            <label for="">New Password</label>
                            <input type="password" class="form-control" name="password" placeholder="leave blank if you wont change it">
                        </div>
                        <div class="form-group">
                            <label for="">Confirm Password</label>
                            <input type="password" class="form-control" name="password_confirmation" placeholder="leave blank if you wont change it">
                        </div>
                    </div>

                    <div class="box-footer clearfix">
                      <div class="pull-right">
                        <button class="btn bg-olive btn-flat" name="submit" value="1">Save Changes</button>
                      </div>
                    </div
                </div>
            </div>
        </form>
    </div>
</section>

@endsection