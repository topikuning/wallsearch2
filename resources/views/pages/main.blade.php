@extends('home')

@section('content')
	
	<section class="home-search">
    <div class="pattern"></div>

    <div class="vcontainer">
      <div class="container">
		<div align="center">
			@if(config('site.enable_ads'))
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="{{config('site.ad_client')}}"
					 data-ad-slot="{{config('site.ad_slot')}}"
					 data-ad-format="auto"></ins>
				<script data-cfasync="false">
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			@endif
		</div>
		<h1 class="page-title">{{ config('site.main_title') }}</h1>
        <form class="form-inline" action="/search" method="get">
          <div class="form-group">
            <input type="text" class="form-control input-lg search-text" name="s" id="s" placeholder="e.g. {{ config('site.search_term') }}">
          </div>

          <button type="submit" class="btn btn-default btn-lg btn-primary">Search</button>
        </form>

      </div>
    </div>
  </section>

@endsection