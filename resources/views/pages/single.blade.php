@extends('home')
@section('content')

    <?php
		$url = ('/wp-content/uploads/'. $post->slug . '.jpg');
    ?>
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $current_title }}" />
    <meta property="og:url" content="{{ Request::url() }}"/>
    <meta property="og:site_name" content="{{ config('site.site_title') }}" />
    <meta property="og:description" content="{{ ucfirst(strtolower(implode(' ', $body))) }}"/>
    <meta property="og:image" content="{{ $url }}" />
		<section class="blog-section">
  <div class="container">

    <div class="blog-page-section">
      <div class="blog-area animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="400">

        <div class="blog-post-title">
          <h1>{{ $post->title }}</h1>

          <div class="row">
            <div class="col-sm-8 mb-30">
				<div align="center">
					@if(config('site.enable_ads'))
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
							 data-ad-format="horizontal"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					@endif
				</div>
				<hr color="#eee"/>
              <div class="entry-content">
                <p><img src="{{ $url }}" class="img-responsive" alt="{{ $post->keyword .' - '. $post->title }}" /></p>
              </div>
				<hr color="#eee"/>
				<div align="center">
					@if(config('site.enable_ads') && $desktop)
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
							 data-ad-format="rectangle"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					@endif
				</div>
				<hr color="#eee"/>
              <div class="entry-meta">
                <span class="tag-links">
					<a href="{{ ('/category/'.$post->slug_keyword.'.html') }}" rel="follow">{{ $post->keyword }}</a>
					@foreach($tags as $tag)
						<a href="{{ ('/'.$tag->slug.'-'.$tag->id.'.html') }}" rel="follow">{{ $tag->title }}</a>
					@endforeach
                </span>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="details-wrap">
                <h3 class="widget-title nomt">Download.</h3>

                <ul class="photo-details">
                  <li>
                    <div class="device-title">iPhone / Android</div>
                    <ul class="list list-inline list-icons">
                      <li><a href="{{ ('/download/320/480/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">320x480</a></li>
                      <li><a href="{{ ('/download/640/960/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">640x960</a></li>
                      <li><a href="{{ ('/download/640/1136/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">640x1136</a></li>
                    </ul>
                  </li>

                  <li>
                    <div class="device-title">iPad / Tablet</div>
                    <ul class="list list-inline list-icons">
                      <li><a href="{{ ('/download/1024/1024/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">1024x1024</a></li>
                      <li><a href="{{ ('/download/640/960/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">640x960</a></li>
                      <li><a href="{{ ('/download/640/1136/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">640x1136</a></li>
                    </ul>
                  </li>

                  <li>
                    <div class="device-title">Desktop</div>
                    <ul class="list list-inline list-icons">
                      <li><a href="{{ ('/download/1024/768/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">1024x768</a></li>
                      <li><a href="{{ ('/download/1152/864/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">1152x864</a></li>
                      <li><a href="{{ ('/download/1280/960/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">1280x960</a></li>
                      <li><a href="{{ ('/download/1600/1200/'. $post->code) }}" rel="noindex,nofollow" target="_blank" class="playlist-button">1600x1200</a></li>
                    </ul>
                  </li>
                </ul>
				<div align="center">
					@if(config('site.enable_ads') && !$desktop)
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
							 data-ad-format="rectangle"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					@endif
				</div>
				<hr color="#eee"/>
                <h3 class="widget-title">Custom Dimensions.</h3>
                <form class="form-horizontal" target="_blank" action="/rsz" method="get">
                  <div class="form-group">
                    <label for="w" class="col-sm-2 control-label">Width</label>
                    <div class="col-sm-10">
                      <input type="text" name="w" id="w" class="form-control">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="h" class="col-sm-2 control-label">Height</label>
                    <div class="col-sm-10">
                      <input type="text" name="h" id="h" class="form-control">
					  <input type="hidden" name="c" id="c" value="{{$post->code}}">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="d" class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-10">
                      <button type="submit" class="btn btn-default btn-primary">Download</button>
                    </div>
                  </div>
                </form>
				
				<h3 class="widget-title">Share.</h3>
                <div class="social-icons">
                  <a onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + location.href, 'sharer', 'width=626,height=436');" href="#" rel="noindex,nofollow" class="social-link" target="_blank"> <i class="fa fa-facebook"></i></a>
                  <a onclick="window.open('https://twitter.com/home?status=' + location.href, 'sharer', 'width=626,height=436');" href="#" rel="noindex,nofollow" class="social-link" target="_blank"> <i class="fa fa-twitter"></i></a>
                  <a onclick="window.open('https://plus.google.com/share?url=' + location.href, 'sharer', 'width=626,height=436');" href="#" rel="noindex,nofollow" class="social-link" target="_blank"> <i class="fa fa-google"></i></a>
                  <a onclick="window.open('http://pinterest.com/pin/create/button/?url=' + location.href + '&description={{$post->title}} | {{$post->keyword}}&media={{$url}}', 'sharer', 'width=626,height=436');" href="#" rel="noindex,nofollow" class="social-link" target="_blank"> <i class="fa fa-pinterest"></i></a>
                </div>

              </div>
            </div>
          </div>

        </div><!-- .blog-post-title -->
      </div><!-- .blog-area -->
		<div id="disqus_thread"></div>
		<script>
		var disqus_config=function(){this.page.url="{{ Request::url()}}",this.page.identifier="{{$post->code}}"};!function(){var e=document,t=e.createElement("script");t.src="//topikuning.disqus.com/embed.js",t.setAttribute("data-timestamp",+new Date),(e.head||e.body).appendChild(t)}();
		</script>
		<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    </div><!-- .blog-page-section -->
    @include('partials._related')
	<!-- .related-posts -->

  </div>
</section>
@endsection