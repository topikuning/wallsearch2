@extends('home')

@section('content')
	<section class="serp-search">
    <div class="pattern"></div>
    <div class="vcontainer">
      <div class="container">
        <form class="form-inline" action="/search" method="get">
          <div class="form-group">
            <input type="text" class="form-control input-lg search-text" name="s" id="s" placeholder="e.g. {{ config('site.search_term') }}" value="<?php echo filter_input(INPUT_GET, 's') ?>">
          </div>

          <button type="submit" class="btn btn-default btn-lg btn-primary">Search</button>
        </form>

      </div>
    </div>
  </section><!-- .serp-search -->

  <section class="services-section clearfix">
    <div class="container">
      <div class="text-center animate clearfix" data-anim-type="zoomIn" data-anim-delay="400">
        <h1 class="heading animate fadeInUp"><?php echo ucwords(filter_input(INPUT_GET, 's')) ?></h1>
        <div class="pagetitle-separator animate fadeInRight"></div>
		<div align="center">
			@if(config('site.enable_ads'))
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="{{config('site.ad_client')}}"
					 data-ad-slot="{{config('site.ad_slot')}}"
					 data-ad-format="auto"></ins>
				<script data-cfasync="false">
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			@endif
		</div>
      </div>

      <div class="row">
        <div id="gallery">
			<?php $listing = $posts; ?>
			@include('partials._posts')
        </div>
      </div>
		<hr color="#eee"/>
			<div align="center">
				@if(config('site.enable_ads'))
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="{{config('site.ad_client')}}"
						 data-ad-slot="{{config('site.ad_slot')}}"
						 data-ad-format="auto"></ins>
					<script data-cfasync="false">
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				@endif
			</div>
		<hr color="#eee"/>

	  
	  <div class="row">
  	    <div class="col-md-12">
    			<ul class="course-scroll-btn">
					<?php
						$ispage = $posts->currentPage();
						$prev = $ispage -1;
						$next = $ispage + 1;
					?>
					@if($ispage > 1)
						<li><a class="course-prev" href="{{ url('/search?s='. urlencode($search) .'&page='. $prev) }}" rel="noindex,follow" data-slide="prev"></a></li>
					@endif
					@if($posts->hasMorePages())
						<li><a class="course-next" href="{{ url('/search?s='.urlencode($search) .'&page='. $next) }}" rel="noindex,follow" data-slide="next"></a></li>
					@endif
    			</ul>
    		</div>
      </div>
    </div>
  </section><!-- .services-section -->


@endsection
