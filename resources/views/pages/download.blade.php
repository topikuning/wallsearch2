@extends('home') 
@section('content')
            <div class="post-container">
              <div class="post-content">
                <!-- begin:article -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="blog-title">
                      <div class="meta-date">
                        <span class="meta-date-day">{{ $post->created_at->format('d') }}</span> 
                        <span class="meta-date-month">{{ $post->created_at->format('M') }}</span>
                        <span class="meta-date-year">{{ $post->created_at->format('Y') }}</span>
                      </div>
                      <h2><a href="{{ url($post->slug_keyword.'/'.$post->slug) }}.html">{{ $post->title }}</a></h2>
                     <small><a href="#">{{ $post->url }}</a></small>
                      | <small>in <a href="{{ url($post->slug_keyword) }}">{{ $post->keyword }}</a></small>
                    </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4>Ads here</h4>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center" style="margin-top:10px;margin-bottom:10px;line-height:50px;">
                                <?php $ext = pathinfo($post->image_url, PATHINFO_EXTENSION); ?>
                                <h2 id="counter-download" data-href="{{ url('assets/fullimage/'. $post->slug . '.' .$ext.'?download=true') }}">10</h2>
                                <p>Preparing image {{ $post->title }} please wait..</p>
                            </div>
                            
                        </div>

                      </div>


                  </div>
                </div>
                <!-- end:article -->

                <!-- begin:related-portfolio -->
                <div class="row">
                  <div class="col-md-12" style="margin-top:30px;">
                    <div class="post-container">
                      <div class="post-content">

                        <div class="row">
                          <div class="col-md-12">
                            <div class="heading-title">
                              <h3 class="text-uppercase">You may also like</h3>
                            </div>
                          </div>
                        </div>
                        <!-- break -->

                        <div class="row wrapper-portfolio">
                          @include('partials._related')
                        </div>
                        <!-- break -->

                      </div>
                    </div>
                  </div>
                </div>
                <!-- end:related-portfolio -->

              </div>
            </div>
@endsection