<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;


class RssController extends Controller {

    public function index()
    {
 
        $now = Carbon::now();
        $feed = new Feed();
        $channel = new Channel();
        $channel
          ->title(config('site.site_title'))
          ->description(config('site.site_desc'))
          ->url(url())
          ->language('en')
          ->lastBuildDate($now->timestamp)
          ->appendTo($feed);

        $posts = DB::table('posts')->where('created_at','<=', DB::raw('now()'))
                 ->take(20)->get();

        foreach ($posts as $key => $post) {
          $pubDate = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);
          $url = url($post->slug.'-'.$post->id.'.html');
          $item = new Item();
          $item
            ->title($post->title)
            ->description($this->getSingle($post))
            ->url($url)
            ->pubDate($pubDate->timestamp)
            ->guid($url, true)
            ->appendTo($channel);
        }

       $feed = (string)$feed;

        // Replace a couple items to make the feed more compliant
        $feed = str_replace(
          '<rss version="2.0">',
          '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">',
          $feed
        );
        $feed = str_replace(
          '<channel>',
          '<channel>'."\n".'    <atom:link href="'.url('/rss').
          '" rel="self" type="application/rss+xml" />',
          $feed
        );

          return response($feed)
              ->header('Content-type', 'application/rss+xml');
    }

    public function getSingle($post)
    {
 
        $post_random = explode(',', $post->post_random);
        $post->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);

        $query = 'SELECT * FROM posts WHERE id IN('.$post->post_random.') ORDER BY FIELD(id, '.$post->post_random.')';
        $details = DB::select($query);

        $body = [];
        $desc = [];

        foreach ($details as $key => $value) {
            if($key < 4){
                $body[] = $value->title;
            } 
            if($key < 3){
                $desc = $body;
            }       
        }

        return view('pages.single_rss',[
            'post' => $post,
            'body' => $body
        ])->render();

 
    }
}