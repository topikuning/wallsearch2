<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;



class CategoryController extends Controller {

    public function index($slug)
    {
        $posts = DB::table('posts')
                    ->where('published','1')
                    ->where('slug_keyword', $slug)
                    ->get();
 
        if(!count($posts)){
            abort(404);
        }

        $category = "";
        $titles = "";
        $desc = "";
        $index = 0; 
        foreach($posts as $row){
            if($index < 5){
                $separator =  ", "; 
                if($index > 2) $separator = ". ";
                if($titles ==""){
                    $titles = $row->title;
                }else{
                    $titles .= $separator . $row->title;
                }

                if($index < 3) $desc = $titles;
            }

            $category = $row->keyword;
            $index++;
        }

        $now = Carbon::now();
        $tmp = []; 
        foreach($posts as $row){
            $created_at = Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at); 
            if($created_at->lte($now)){
                $tmp[] = $row;
            }
        }
  
        return view('pages.category',
            [
             'posts'=> $tmp,
             'body' => $titles,
             'category' => $category,
             'current_title' => $category,
             'current_description' =>  $category .'. '.$desc .', ' . config('site.site_title')
             ]
        );
    }

}