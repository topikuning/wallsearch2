<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->group(['prefix' => 'badmin', 'middleware' => 'auth.admin', 'namespace' => 'App\Http\Controllers\Admin'], function() use ($app){
    $app->get('/createSlug', 'TagController@createSlug');
    $app->get('/','DashBoardController@index');
    
    $app->get('/pages','PageController@index');
    $app->get('/pages/create','PageController@create');
    $app->post('/pages/create','PageController@store');
    $app->get('/pages/read', 'PageController@read');
    $app->get('/pages/edit/{id}', 'PageController@edit');
    $app->delete('/pages/{id}', 'PageController@destroy');
    $app->post('/pages/attribute','PageController@postEditAtrribute');

    $app->get('/spinner','SettingController@index'); 
    $app->post('/spinner','SettingController@store');

    $app->get('/profile','UserController@getProfile'); 
    $app->post('/profile','UserController@postProfile'); 

});



$app->group(['namespace' => 'App\Http\Controllers'], function() use ($app) {
    $app->get('/', 'HomeController@index');


    $app->get('/auth/login', 'AuthController@getLogin');
    $app->post('/auth/login', 'AuthController@postLogin');
    $app->get('/auth/logout', 'AuthController@getLogout');
    $app->get('auth/register', 'AuthController@getRegister');
    $app->post('auth/register', 'AuthController@postRegister');

    $app->get('/feed','RssController@index');
    $app->get('/page/{slug}.html', 'HomeController@getPage');
    $app->get('/page/{page}', 'HomeController@index'); 
    $app->get('/index.xml','SiteMapController@index');
    $app->get('/index-img.xml','SiteMapController@image');
    $app->get('/index/category-{code}.xml','SiteMapController@posts');
    $app->get('/index/images-{page}.xml','SiteMapController@images');

    $app->get('/rsz', 'ImageController@downloadCustomImage');
	$app->get('/search','HomeController@searchData');
	$app->get('/wp-content/uploads/thumb/{slug}','ImageController@getImage');
    $app->get('/wp-content/uploads/{slug}','ImageController@getFullImage');
	$app->get('/download/{width}/{height}/{slug}', 'ImageController@downloadImage');
    
	$app->get('/{slug}.html', 'HomeController@getSingle');
    $app->get('/category/{keyword}.html','CategoryController@index');
});
