<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB; 
use Storage;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class GeneratePost extends Command {

  protected $name = 'content:scrap';

  protected $description = 'Run Scrapping data based on keywords table';
  protected static $curlDataWritten = 0;
  protected static $curlFH = false;

  public function __construct()
  {
    parent::__construct();
  }

  public function multiRequest($data, $options = array(), $useagent=false) {


      $agents = [
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.5 (KHTML, like Gecko) Chrome/2.0.173.1 Safari/530.5",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.558.0 Safari/534.10",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML,like Gecko) Chrome/9.1.0.0 Safari/540.0",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.600.0 Safari/534.14",
        "Mozilla/5.0 (X11; U; Windows NT 6; en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.587.0 Safari/534.12",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.11 Safari/534.16",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
      ];
   
      // array of curl handles
      $curly = array();
      // data to be returned
      $result = array();
     
      // multi handle
      $mh = curl_multi_init();
     
      // loop through $data and create curl handles
      // then add them to the multi-handle
      foreach ($data as $id => $d) {
     
        $curly[$id] = curl_init();
     
        $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
        curl_setopt($curly[$id], CURLOPT_URL,            $url);
        curl_setopt($curly[$id], CURLOPT_HEADER,         0);
        curl_setopt($curly[$id], CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curly[$id], CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curly[$id], CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
        if($useagent){
          curl_setopt($curly[$id],CURLOPT_USERAGENT,$agents[array_rand($agents)]);
        }
        // post?
        if (is_array($d)) {
          if (!empty($d['post'])) {
            curl_setopt($curly[$id], CURLOPT_POST,       1);
            curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
          }
        }
     
        // extra options?
        if (!empty($options)) {
          curl_setopt_array($curly[$id], $options);
        }
     
        curl_multi_add_handle($mh, $curly[$id]);
      }
     
      // execute the handles
      $running = null;
      do {
        curl_multi_exec($mh, $running);
      } while($running > 0);
     
     
      // get content and remove handles
      foreach($curly as $id => $c) {
        $result[$id] = curl_multi_getcontent($c);
        curl_multi_remove_handle($mh, $c);
      }
     
      // all done
      curl_multi_close($mh);
     
      return $result;
  }

  public function getGoogleSuggest($keyword){

    $result = []; 
    if(config('site.g_suggest')){
        $this->info('use google suggest  to insert keyword');
        $url = config('site.node_server_url'). '/output.json?'; 
        $params = ['site' => 'gsuggest', 'keyword' => $keyword];
        $url = $url . http_build_query($params);
        $this->info('get Google Suggest '. $url); 
        $response = $this->multiRequest([$url]); 

        return json_decode($response[0]);
    }

    return $result;

  }

  public function fire()
  {
    $this->info("run scrapping command..");
    $this->sampleData(); 
    $this->info("done..");
    //$this->call('sitemap:update');
  }

  private function sampleData()
  { 
      $last_index = '0';
      if(Storage::exists('last_keyword_id')){
        $last_index = Storage::get('last_keyword_id'); 
      }
      
      $limit_scrap = config('site.keywords_per_scrap','5');
      $this->info('get data from keywords table where id > '. $last_index . ' limit '. $limit_scrap);

      $data = DB::table('keywords')
              ->where('id', '>', $last_index)
              ->orderBy('id')
              ->take($limit_scrap)
              ->get();



      /**
      if(count($data)==1){
        $this->error('only one keyword record found.. we need at least 2 records to randomize content');
        return [];
      }
      **/

      $failed_scrap = false;
      foreach($data as $row){
        if(!$failed_scrap){
            $this->info('start or resume scrap script -------------');
            $keyword = $row->keyword;
            $json_data = $this->scrapData($keyword);
            if($json_data === false){
              $failed_scrap = true;
            }else{
              $last_index = $row->id;
              $this->generateData($json_data);
              $this->info('');
              $this->info('update last keyword id .. '. $last_index);
              Storage::put('last_keyword_id', $last_index);
              $this->info('delay 5 seconds-------------');
              $this->info('');
              //sleep(5);
            }
        }

      }

  }

  private function scrapData($keyword)
  {
      $this->info('');
      $engine = $this->option('engine');
      if(!$engine){
        $engine = config('site.engine_scrap');
      }
      $this->info('starting grap keyword '. $keyword.' from search engine..');
      $this->info('use '. $engine . ' engine');
      $url = config('site.node_server_url') . '/output.json?site='. $engine .'&keyword='. urlencode($keyword);
      $this->info('get data from '. $url);

      $ch = curl_init($url);
      curl_setopt_array(
        $ch,
        array(
           CURLOPT_SSL_VERIFYPEER => false,
           CURLOPT_RETURNTRANSFER  => true
        )
      );
      $content = curl_exec($ch);
      curl_close($ch);
      if($content){
        $data = json_decode($content); 
        $this->info('found '. count($data). ' data');
        $this->info('convert object to array');
        $arr = [];
        $keyword = $this->safe_string_insert($keyword, 'title');
        foreach($data as $row){
          if($row->title){
              $en_title = $this->safe_string_insert($row->title, 'title');
              $en_body = $this->safe_string_insert($row->body, 'desc');
              if(isset($row->thumb_url))
              $arr[] =[
                'title' => $en_title,
                'body' => $en_body,
                'keyword' => $keyword,
                'slug_keyword' => str_slug($keyword),
                'thumb_url' => $row->thumb_url,
                'url' =>$row->image->surl,
                'image_url' => $row->image->imgurl
              ];
          }

        }

        $suggest = $this->getGoogleSuggest($keyword); 
        foreach ($suggest as $key => $value) {
          $found = DB::table('keywords')->where('keyword', 'like', $value->result)->first(); 
          if(!$found){
            $this->info('inserting new keyword from google suggest '. $value->result);
            $this->info('------google suggest--------');
             DB::table('keywords')
                 ->insert([
                    'keyword' => $value->result, 
                    'created_at' => DB::raw('now()')
                  ]);
          }
        }

        return $arr;
      }else{
        $this->error('failed to communicate with node webservice. please check your nodejs server is running?');
        return false;
      }
      $this->info('');

  }

  private function createUniquRandomKey($table)
  {
    $newslug = strtolower(str_random(16));
    do {
      $exist = DB::table($table)->where('code','like',$newslug)->first();
      if($exist){
        $this->info('code '. $newslug.' exists, create new one'); 
        $newslug = str_random(16);
      }
    } while ($exist);

    return $newslug;
  }

  private function generateData($data)
  {
    //$data = $this->sampleData(); 
    
    //$this->info('shuffle data'); 
    //shuffle($data);
    $this->info('insert into posts table');
    $this->info('total data will be inserted.. '. count($data));

    
    $data_array = [];
    $limit = 12;
    $index = 0;

    $random_hour = rand(1,24);
	if(config('site.back_date')){
		$randomday=rand(3,100);
		$date_keyword = Carbon::now()->subDays($randomday);
	} else {
		$date_keyword = Carbon::now()->addHours($random_hour);
	}
    $date_post_format = $date_keyword->format('Y-m-d H:i:s');


    foreach($data as $row){
      $row['title'] = trim($row['title']);
      $row['title'] = str_replace('...', '', $row['title']);

      $this->info('insert data with title '. $row['title']);
      $row['slug'] = str_slug($row['title']); 
      $keyword = $row['keyword'];
		
		if(config('site.back_date')){
		  $randomhp=rand(1,10);
		  $random_hour_post = rand($random_hour, ($random_hour + $randomhp));
		} else {
			$random_hour_post = rand($random_hour, ($random_hour + 12));
		}
      $date_post =  Carbon::createFromFormat('Y-m-d H:i:s',$date_post_format)
                    ->addHours($random_hour_post)->addMinutes(rand(1,60));
      $row['created_at'] = $date_post->format('Y-m-d H:i:s'); 


      
     if(DB::table('posts')->where('slug','like',$row['slug'])->first()){
        $this->info('duplicate title, ignored');
      }else{
        if(!$this->badWord($row['title']) && ($index < $limit)){
            $contentType = $this->getResponseImage($row['image_url']);
            $arr = explode('/', $contentType); 
            if(count($arr)==1) $contentType = false;
             if($contentType){
               if($index < $limit){
                  $ratingCount = rand(0,1000);
                  $ratingValue = rand(0,100); 
                  $row['thumb_url'] = $this->save_thumb($row['thumb_url'], $row['slug']); 
                  $row['image_url'] = $this->save_large($row['image_url'], $row['slug'], $contentType);
                  $row['ratingCount'] = $ratingCount; 
                  $row['ratingValue'] = $ratingValue;
                  $row['code'] = $this->createUniquRandomKey('posts');
                  $row['local_image'] = config('site.local_image')?'1':'0';
                  $id = DB::table('posts')->insertGetId($row);
                  $row['id'] = $id;
                  $data_array[] = $row;
                  $this->info('success inserting data.. ');              
               }
               $index++;              
             }else{
              $this->error('Image is not valid, ignored');
             }
        }else{
            $this->info('content contain badwords or reach limit post, ignored');
          }

      }




      $this->info('-----end read record--------------');
      
    }

    if(count($data_array)){
      $first = $data_array[0]; 
      $rand = $data_array; 
      shuffle($rand); 
      $last = $rand[0]['id'];
      if(!DB::table('post_keyword')->where('slug',$first['slug_keyword'])->first()){
        $this->info('insert post keyword title');
        $insert = [
          'title' => $first['keyword'],
          'slug' => $first['slug_keyword'],
          'body' => $first['keyword'],
          'last_post' => $last,
          'code' => $this->createUniquRandomKey('post_keyword'),
          'created_at' => $date_keyword->format('Y-m-d H:i:s')
        ];
        $post_id = DB::table('post_keyword')->insertGetId($insert);

        DB::table('posts')
            ->where('id',$last)
            ->update(['created_at' => $date_keyword->subMinutes(10)->format('Y-m-d H:i:s')]);

        $ids = []; 
        foreach ($data_array as $key => $value) {
          $ids[] = $value['id'];
        }


        foreach ($data_array as $key => $value) {
          if($key > 0){
            shuffle($ids);
          }
          DB::table('posts')
              ->where('id',$value['id'])
              ->update(['post_id' => $post_id, 'post_random' => implode(',',$ids)]);
        }
        $this->info('inserting post keyword');
      }
    }

  }

  private function insertCategories($categories)
  {
    if(trim($categories)=="") return false; 
    $categories = trim($categories);
    $arr = explode(',', $categories); 
    $data = [];
    $this->info('inserting categories..');
    $category_id = '0';
    foreach($arr as $row){
      $row = trim($row);
      $this->info('inject category: ' . $row);
      $cat_exist = DB::table('categories')->where('title','like', $row)->first(); 
      if($cat_exist) {
        $this->info('category exist.. use exiting data');
        $category_id = $cat_exist->id;
      }else{
        $this->info('category does not exist.. creating new one');
        $category_id = $this->insertCategory($row);
      }

      return $category_id;

    }

  }

  private function insertCategory($title){
    $data = [
      'title' => $title,
      'slug' => str_slug($title),
      'created_at' => DB::raw('now()')
    ];

    return DB::table('categories')->insertGetId($data);
  }

  private function save_thumb($url, $slug)
  {
    if(!config('site.local_image')) return $url; 
    $this->info('saving thumbnail, please wait..');
    $path = base_path(). '/public/images/thumb/'. $slug . '-thumb.jpg'; 
    $this->save_image($url, $path);
    return $slug .'-thumb.jpg';
  }

  public function save_large($url, $slug, $contentType)
  {
    if(!config('site.local_image')) return $url;
    $arr = explode('/', $contentType); 
    $ext = $arr[1];
    if($ext=='jpeg') $ext = 'jpg';
     
    $name = $slug .'-large.'. $ext; 
    $this->info('saving image.. please wait..');
    $path = base_path() . '/public/images/large/'. $name; 
    if(config('site.thintumb_url')){
      $url = config('site.thintumb_url') .'?q=40&d=1&src=' . urlencode($url);
      $this->info('use exiting thintumb to downgrade the image '.$url);
    }
    $this->save_image($url, $path);
    if(!config('site.thintumb_url')){
      sleep(2);
      $this->info('downgrade image quality, please wait..');
      Image::configure(array('driver' => 'imagick'));
      $img = Image::make($path); 
      $img->save($path,40);      
    }
    return $name;
  }

    public static function curlWrite($h, $d){
        fwrite(self::$curlFH, $d);
        self::$curlDataWritten += strlen($d);
        if(1==2){
            return 0;
        } else {
            return strlen($d);
        }
    }

  private function save_image($url, $path)
  {
        $tmpfile = $path;

        $fp = fopen ($tmpfile, 'w+');//This is the file where we save the    information
        $ch = curl_init($url);//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.122 Safari/534.30');
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);
  
        /**
        self::$curlFH = fopen($tmpfile, 'w');
        self::$curlDataWritten = 0;
        $curl = curl_init($url);
        curl_setopt ($curl, CURLOPT_TIMEOUT, 20);
        curl_setopt ($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.122 Safari/534.30");
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt ($curl, CURLOPT_HEADER, 0);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt ($curl, CURLOPT_WRITEFUNCTION, 'App\Console\Commands\GeneratePost::curlWrite');
        @curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, true);
        @curl_setopt ($curl, CURLOPT_MAXREDIRS, 10);
        
        $curlResult = curl_exec($curl);
        fclose(self::$curlFH);
        **/

  }

  private function getResponseImage($url)
  {
      // Assume failure.
      $result = -1;
      $success = false;
      $userAgent = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0';

      $this->info('test the image url '. $url);

      $curl = curl_init( $url );

      // Issue a HEAD request and follow any redirects.
      curl_setopt( $curl, CURLOPT_NOBODY, true );
      curl_setopt( $curl, CURLOPT_HEADER, true );
      curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,15);
      curl_setopt($curl, CURLOPT_TIMEOUT, 10);
      curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
      curl_setopt( $curl, CURLOPT_USERAGENT, $userAgent );
      curl_setopt( $curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
      $data = curl_exec( $curl );
      $contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
      curl_close( $curl );

      if( $data ) {
        $content_length = "unknown";
        $status = "unknown";

        if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
          $status = (int)$matches[1];
        }

        if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
          //$content_length = (int)$matches[1];
        }

        // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        if( $status == 200 || ($status > 300 && $status <= 308) ) {
          $this->info($data);
          if( preg_match( "/Content-Type: (\bimage\b)/", $data, $matches ) ) {
              $success = $contentType;
          }

        }
      }

      return $success;
  }

  private function badWord($str)
  {
    $bad = "death,dead,deceased,demise,die,dying,expire,fatal stroke,past away,perished,cocaine,drugs,heroin,marijuana,medication,morphine,overdose,oxycodone,oxycontin,pharmaceutical,pharmacy,shit,piss,fuck,tits,drown,drowned,electrocuted,electrocution,execution,killed,killer,manslaughter,miscarriage,murder,murdered,poisoned,poisoning,slaughtered,strangler,strangulation,suffocation,suicide,abortion,aneurysm,bum,crash,cancer,cerebral accident,desanguinated,disfigured,embolism,hemorrhage,horror,maimed,paralyzed,stroke,erection,masturbation,pedophile,penis,porn,pussy,squirting,pussy,schlong,sex,xx,xxx,xxxx,squirting,squirt,blowjob,public sex,p0rn,memek,ngentot,itil,kontol,burial,casket,funeral,attack,bomb,bomber,incerated,jail,prison,terrorist,casino,gambling,google,las vegas,video poker,poker,chip poker,ass,asshole,anal,creampie,bukkake,rapid,rapidshare,megaupload,hotfile";
    $arr = explode(',', $bad); 
    $result = false; 
    foreach($arr as $find){
      if(str_contains(strtolower($str), $find)) $result = true;
    }
    return $result;

  }

private function utf8_uri_encode( $utf8_string, $length = 0 ) {
  $unicode = '';
  $values = array();
  $num_octets = 1;
  $unicode_length = 0;

  $string_length = strlen( $utf8_string );
  for ($i = 0; $i < $string_length; $i++ ) {

    $value = ord( $utf8_string[ $i ] );

    if ( $value < 128 ) {
      if ( $length && ( $unicode_length >= $length ) )
        break;
      $unicode .= chr($value);
      $unicode_length++;
    } else {
      if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

      $values[] = $value;

      if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
        break;
      if ( count( $values ) == $num_octets ) {
        if ($num_octets == 3) {
          $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
          $unicode_length += 9;
        } else {
          $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
          $unicode_length += 6;
        }

        $values = array();
        $num_octets = 1;
      }
    }
  }

  return $unicode;
}

private function seems_utf8($str) {
  $length = strlen($str);
  for ($i=0; $i < $length; $i++) {
    $c = ord($str[$i]);
    if ($c < 0x80) $n = 0; # 0bbbbbbb
    elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
    elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
    elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
    elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
    elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
    else return false; # Does not match any model
    for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
      if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
        return false;
    }
  }
  return true;
}

private function url_title($title, $separator = '-', $capitalize = FALSE)
{
  $title = strip_tags($title);
  $title = str_replace(array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"), '', $title);
  // Preserve escaped octets.
  $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
  // Remove percent signs that are not part of an octet.
  $title = str_replace('%', '', $title);
  // Restore octets.
  $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

  if ($this->seems_utf8($title)) {
    if (function_exists('mb_strtolower')) {
      $title = mb_strtolower($title, 'UTF-8');
    }
    $title = $this->utf8_uri_encode($title, 200);
  }

  $title = preg_replace('/\`\~\!\@\#\$\%\^\&\*\(\)\_\+\-\=\<\>\?\:\"\{\}\|\,\.\/\;\[\]/', '', $title);
  $title = str_replace('.', $separator, $title);
  $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
  $title = ($capitalize) ? safe_ucwords($title): $title;
  $title = preg_replace('/\s+/', $separator, $title);
  $title = preg_replace('|-+|', $separator, $title);
  $title = rtrim($title, $separator);
  $title = trim($title);
  $title = stripslashes($title);
  $title = ($capitalize) ? $title: $this->safe_strtolower($title);
  $title = urldecode($title);
  //$title = html_entity_decode($title, ENT_QUOTES, "UTF-8");
  return $title;
}

private function permalink_url ($str, $replace_separator_to_space = false)
{
  $str = $this->url_title($str, '-', false);
  
  if ($replace_separator_to_space)
  {
    $str = str_replace(config('separator'), ' ', $str);
  }

  return $str;
}

private function safe_string ($str)
{
  $str = $this->url_title($str, '-');
  $str = str_replace('-', ' ', $str);
  return $str;
}

private function safe_ucfirst($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_strtoupper'))
    {
      $encoding = "UTF-8";
      $strlen = mb_strlen($str, $encoding);
      $first_char = mb_substr($str, 0, 1, $encoding);
      $then = mb_substr($str, 1, $strlen - 1, $encoding);
      $str = mb_strtoupper($first_char, $encoding) . $then;
    }
  }
  else
  {
    $str = ucfirst($str);
  }

  return $str;
}

private function safe_strtolower($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_convert_case'))
    {
      $str = mb_convert_case($str, MB_CASE_LOWER, "UTF-8");
    }
  }
  else
  {
    $str = strtolower($str);
  }

  return $str;
}

private function safe_ucwords($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_convert_case'))
    {
      $str = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    }
  }
  else
  {
    $str = ucwords($str);
  }

  return $str;
}

private function safe_string_insert ($str, $type)
{
  $str = $this->title_case($this->safe_string($str));

  switch ($type) {
    case 'title':
      if (strlen($str) < 3)
      {
        $str = "Untitled Document";
      }
      else if (strlen($str) > 3 && strlen($str) < 8)
      {
        $str = "Document " . $str;
      }
      break;
    case 'desc':
      if (strlen($str) < 3)
      {
        $str = "No Description";
      }
      else if (strlen($str) > 3 && strlen($str) < 8)
      {
        $str = "Document " . $str;
      }
      break;
  }

  return $str;
}

private function bersihkata($kata)
{
  $kata = strip_tags($kata);
  $kata = $this->safe_strtolower($kata);
  $kata = $this->clean_words($kata);
  $kata = $this->permalink_url($kata, ' ');
  $kata = $this->safe_ucwords($kata);
  return $kata;
}

private function title_case ($title)
{
  $regx = '/<(code|var)[^>]*>.*?<\/\1>|<[^>]+>|&\S+;/';
  preg_match_all($regx, $title, $html, PREG_OFFSET_CAPTURE);
  $title = preg_replace ($regx, '', $title);
  $q_left = chr(8216);
  $q_right = chr(8217);
  $double_q = chr(8220);

  preg_match_all ('/[\w\p{L}&`\''. $q_left . $q_right .'"'. $double_q .'\.@:\/\{\(\[<>_]+-? */u', $title, $m1, PREG_OFFSET_CAPTURE);
  foreach ($m1[0] as &$m2) {
    list ($m, $i) = $m2;
    $i = mb_strlen (substr ($title, 0, $i), 'UTF-8');
    
    $m = $i>0 && mb_substr ($title, max (0, $i-2), 1, 'UTF-8') !== ':' && 
      !preg_match ('/[\x{2014}\x{2013}] ?/u', mb_substr ($title, max (0, $i-2), 2, 'UTF-8')) && 
       preg_match ('/^(a(nd?|s|t)?|b(ut|y)|en|for|i[fn]|o[fnr]|t(he|o)|vs?\.?|via)[ \-]/i', $m)
    ? mb_strtolower ($m, 'UTF-8')
    : ( preg_match ('/[\'"_{(\['. $q_left . $double_q .']/u', mb_substr ($title, max (0, $i-1), 3, 'UTF-8'))
    ? mb_substr ($m, 0, 1, 'UTF-8').
      mb_strtoupper (mb_substr ($m, 1, 1, 'UTF-8'), 'UTF-8').
      mb_substr ($m, 2, mb_strlen ($m, 'UTF-8')-2, 'UTF-8')
    : ( preg_match ('/[\])}]/', mb_substr ($title, max (0, $i-1), 3, 'UTF-8')) ||
      preg_match ('/[A-Z]+|&|\w+[._]\w+/u', mb_substr ($m, 1, mb_strlen ($m, 'UTF-8')-1, 'UTF-8'))
    ? $m
    : mb_strtoupper (mb_substr ($m, 0, 1, 'UTF-8'), 'UTF-8').
      mb_substr ($m, 1, mb_strlen ($m, 'UTF-8'), 'UTF-8')
    ));
    
    $title = mb_substr ($title, 0, $i, 'UTF-8').$m. mb_substr ($title, $i+mb_strlen ($m, 'UTF-8'), mb_strlen ($title, 'UTF-8'), 'UTF-8');
  }

  foreach ($html[0] as &$tag) $title = substr_replace ($title, $tag[0], $tag[1], 0);
  return $title;
}

  protected function getArguments()
  {
    return [
      //['example', InputArgument::REQUIRED, 
      //  'An example argument.'],
    ];
  }

  protected function getOptions()
  {
    return [
      ['engine', null, InputOption::VALUE_OPTIONAL, 
        'Engine scrap bing or yandex, default is bing', null],
    ];
  }

}