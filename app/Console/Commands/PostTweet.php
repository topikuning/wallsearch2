<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use Storage;
use Abraham\TwitterOAuth\TwitterOAuth;

class PostTweet extends Command {

  protected $name = 'tweet:post';

  protected $description = 'Posting to twitter';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    $this->info("run twitter posing");
    $this->createTweet();
    $this->info("done..");
  }

  private function createTweet()
  {
    $last_id = 0; 
    if(Storage::has('last_tweet')){
      $last_id = Storage::get('last_tweet');  
    }

    $this->info('Get post where id '. ($last_id + 1));
    $post = DB::table('posts')
            ->select('post_keyword.id AS last_id','post_keyword.title AS main_title', 'posts.*')
            ->join('post_keyword', 'post_keyword.last_post', '=', 'posts.id')
            ->where('post_keyword.created_at', '<=', DB::raw('now()'))
            ->where('post_keyword.id', '>', $last_id)->first(); 
    if($post){
      $url = config('site.site_url'). '/'. $post->code .'-'. $post->slug .'.html'; 
      $shorturl = $this->makeUrlShort($url);
      $add_word = config('site.add_word');
      $has_tag = config('site.has_tag');
      shuffle($add_word);
      shuffle($has_tag);
      $emoji = ':)';
      $tweet = $add_word[0]. $post->main_title . ' '.$emoji.' '. $shorturl. ' '. $has_tag[0] . ' '. $has_tag[1]; 
      $media = $post->image_url;
      $this->info('Tweet to twitter with .. ' . $tweet);
      $this->info('use media '. $media);
      $valid_image = $this->getResponseImage($media); 
      Storage::put('last_tweet', $post->last_id);
      if($valid_image){
        $this->generatePost($tweet, $media);  
      }
    }

  }

  private function generatePost($status, $image)
  {
    $consumer_key = env('CONSUMER_KEY'); 
    $consumer_secret = env('CONSUMER_SECRET'); 
    $access_token = env('ACCESS_TOKEN'); 
    $token_secret = env('TOKEN_SECRET'); 
    $connection = new TwitterOAuth(
      $consumer_key, 
      $consumer_secret, 
      $access_token, 
      $token_secret);
    //$content = $connection->get("account/verify_credentials");
      $media1 = $connection->upload('media/upload', array('media' => $image));
      $parameters = array(
          'status' => $status,
          'media_ids' => $media1->media_id_string,
      );
      $result = $connection->post('statuses/update', $parameters); 


  }

  private function makeUrlShort($url){
    $token = env('TOKEN_BIT');
    $link = $this->make_bitly_url($url,$token); 
    return $link;
  }

  /* make a URL small */
  private function make_bitly_url($url,$appkey)
  {
    //create the URL
    $bitly = 'https://api-ssl.bitly.com/v3/shorten?access_token='.$appkey.'&longUrl='. urlencode($url);
    //get the url
    //could also use cURL here
    $response = file_get_contents($bitly);

    $json = json_decode($response); 
    if($json->status_code ==200){
      return $json->data->url;
    }
    return false;
    
  }

  private function getResponseImage($url)
  {
      // Assume failure.
      $result = -1;
      $success = false;
      $userAgent = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0';

      $this->info('test the image url '. $url);

      $curl = curl_init( $url );

      // Issue a HEAD request and follow any redirects.
      curl_setopt( $curl, CURLOPT_NOBODY, true );
      curl_setopt( $curl, CURLOPT_HEADER, true );
      curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
      curl_setopt( $curl, CURLOPT_USERAGENT, $userAgent );
      curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 

      $data = curl_exec( $curl );
      curl_close( $curl );

      if( $data ) {
        $content_length = "unknown";
        $status = "unknown";

        if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
          $status = (int)$matches[1];
        }

        if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
          //$content_length = (int)$matches[1];
        }

        // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        if( $status == 200 || ($status > 300 && $status <= 308) ) {
          $success = true;
        }
      }

      return $success;


  }

  protected function getArguments()
  {
    return [
      //['example', InputArgument::REQUIRED, 
        //'An example argument.'],
    ];
  }

  protected function getOptions()
  {
    return [
      //['file', null, InputOption::VALUE_REQUIRED, 
      //  'file name in folder import.', null],
    ];
  }

}