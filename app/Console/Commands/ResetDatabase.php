<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use Storage;

class ResetDatabase extends Command {

  protected $name = 'content:resetdb';

  protected $description = 'Reset all tables database';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    $this->info("run db reset");

    $reset = $this->argument('resetkeyonly'); 
    if($reset){
      $this->info('Only reset the counter keywords');
      Storage::put('last_keyword_id', '0');
      return true;
    }

    $this->truncateTable('post_keyword');
    $this->truncateTable('recent_viewed');
    $this->truncateTable('posts'); 
    $this->truncateTable('pages');
    $this->truncateTable('categories');
    $this->truncateTable('keywords');
    $this->call('cache:clear');
    $this->info("done..");
  }

  private function truncateTable($table)
  {
    if ($this->confirm('Truncate '.$table.' table? [yes|no]', false)){
        $this->info('--- done truncated '.$table.' table');
        DB::table($table)->truncate();
        if($table=='post_keyword'){
          DB::table('categories_post')->truncate();
          Storage::put('last_sitemap', '0');
        }
        if($table=='keywords'){
          Storage::put('last_keyword_id', '0');
        }
    }else{
      $this->info('aborted');
    }
  }



  protected function getArguments()
  {
    return [
      ['resetkeyonly', InputArgument::OPTIONAL, 
        'only reset the counter of last keyword scrap', null],
    ];
  }

  protected function getOptions()
  {
    return [
      //['file', null, InputOption::VALUE_REQUIRED, 
      //  'file name in folder import.', null],
    ];
  }

}