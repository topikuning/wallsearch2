<?php namespace App\Composers;

use DB;
use stdClass;
use Cache;

class ComposerNavigation {

    public function setSidebar($view)
    {
        $categories = DB::table('categories')
                        ->where('published','1')
                        ->orderBy('title')
                        ->get();
        
        $best_rated = DB::table('post_keyword')
                        ->orderBy('ratingValue','DESC')
                        ->take(6)
                        ->get();
        $last_viewed =  DB::table('post_keyword')
                        ->orderBy('last_viewed','DESC')
                        ->take(5)
                        ->get();

        $data = [
            'categories' => $categories,
            'best_rated' => $best_rated,
            'last_viewed' => $last_viewed
        ];
        $view->with($data);
    }
	
	public function lastView($view)
    {
		$last_viewed = Cache::remember('postsTable_'. env('APP_KEY'), 1440, function()
		{
			return DB::table('posts')
						->where('created_at', '<=', DB::raw('now()'))
                        ->groupBy('keyword')
						->orderByRaw('RAND()')
                        ->take(18)
                        ->get();
		});

        $view->with(['last_viewed' => $last_viewed]);
    }

    public function getPage($view)
    {
		$pages = Cache::remember('pagesTable_'. env('APP_KEY'), 1440, function()
		{
			return DB::table('pages')
                     ->where('published','1')
                     ->orderBy('title')
                     ->get();
		});
        $view->with(['pages' => $pages]);
    }



}